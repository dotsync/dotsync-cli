# dotsync-cli

## Summary
This is the cli project for the dotsync system. The cli allows a user to install the daemon, manage their account, manage their profiles, and configure those profiles. This software is written in rust and will use clap for cli and gRPC for interprocess (daemon) and server calls. The cli will use the UI server to send all account and configuration calls.

## Project Status
This project is in the very early stages of development and is not actively being worked on.

## Installation
To install the cli and start using dotsync go ahead and follow these instructions.

### Windows
Not currently supported.

### Mac
Not currently supported.

### Linux
Currently supported.
To build and install from source:
```
git clone https://gitlab.com/dotsync/dotsync-cli.git
cd dotsync-cli
cargo build --release
chmod +x install-cli
./install-cli
```
To download the current latest release:
```
TODO: RELEASE
cd dotsync-cli
chmod +x install-cli
./install-cli
```

Once you have installed it you verify the installation process by running ```dotsync version```. If everything worked you should have the version you downloaded printing out!

## How to contribute
This software system is fully open source and public. In order to contribute to the development follow the below setup instructions.
Once you have your machine setup and ready pick up an issue and start working!

### Branching convention
For every ticket go ahead and make a branch off of main with the following naming convention:
- For features: feat/[ticket-number]-[ticket-name-dashed-like-this]
- For bug fixes: bug/[ticket-number]-[ticket-name-dashed-like-this]

### Merge Requests
Once you're branch is ready to be merged into main go ahead and squash your commits, update your branch off of main, and make a merge request!
Make sure to include your ticket name and number in the title. Also, write up a good description of the work you did and any important notes in the merge request description. Don't forget to link the issue with the merge request too!

## Testing
All code in this project should be unit (and integration where applicable-- see below!) tested. The CI pipeline is configured to
run automated testing for function, documentation, and formatting. Before making a merge request make sure to run your tests locally, if they fail then your merge requested won't be accepted!

## How to setup development
In order to setup your machine for development and start tweaking the tool follows these instructions.

### Install system dependencies
#### Rust version 1.62
install rust either indivdually or through a version manager like asdf. [asdf-vm](https://asdf-vm.com/)

### Clone the repo (or fork!)
```
git clone https://gitlab.com/dotsync/dotsync-cli.git
cd dotsync-cli
```

### Start coding!
Now that you have rust and the source code you can start tweaking!
