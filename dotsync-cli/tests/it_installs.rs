use assert_cmd::prelude::*;
use predicates::prelude::*;
use std::process::Command;

#[test]
fn it_installs() -> Result<(), Box<dyn std::error::Error>> {
    let mut cmd = Command::cargo_bin("dotsync-cli")?;

    cmd.arg("install");
    cmd.assert()
        .success();

    Ok(())
}
