use clap::{Parser, Subcommand};

#[derive(Subcommand, Debug)]
enum SubCommand {
    /// Check to make sure the cli works
    ItWorks,
    /// Install the background service
    Install,
    /// Uninstall the background service
    Uninstall,
}

#[derive(Parser, Debug)]
#[clap(author = "Christian Smith", version, about)]
/// Small cli for synchronizing files
struct Arguments {
    #[clap(subcommand)]
    /// Subcommand
    subcmd: SubCommand,
}

fn main() {
    let args = Arguments::parse();

    match args.subcmd {
        SubCommand::ItWorks => println!("DotSync works!"),
        SubCommand::Install => println!("Installing background service.."),
        SubCommand::Uninstall => println!("Uninstalling background service..")
    };
}
